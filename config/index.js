export default {
  development: {
    baseUrl: "/api", // 测试接口域名
    basePath: "",
  },
  production: {
    baseUrl: "../../", // 正式接口域名
    basePath: "",
  },
  demo: {
    baseUrl: "/", // 演示
    basePath: "",
  },
}
;
